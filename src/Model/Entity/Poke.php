<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Poke Entity
 *
 * @property int $id
 * @property string $nom
 * @property int $pokedex_number
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $health
 * @property int $attack
 * @property int $defense
 *
 * @property \App\Model\Entity\DresseurPokemon[] $dresseur_pokemons
 */
class Poke extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nom' => true,
        'pokedex_number' => true,
        'created' => true,
        'modified' => true,
        'health' => true,
        'attack' => true,
        'defense' => true,
        'dresseur_pokemons' => true
    ];
}
