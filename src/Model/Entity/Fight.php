<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fight Entity
 *
 * @property int $id
 * @property int $first_dresseur_id
 * @property int $second_dresseur_id
 * @property int $winner_dresseur_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\FirstDresseur $first_dresseur
 * @property \App\Model\Entity\SecondDresseur $second_dresseur
 * @property \App\Model\Entity\WinnerDresseur $winner_dresseur
 */
class Fight extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_dresseur_id' => true,
        'second_dresseur_id' => true,
        'winner_dresseur_id' => true,
        'created' => true,
        'modified' => true,
        'first_dresseur' => true,
        'second_dresseur' => true,
        'winner_dresseur' => true
    ];
}
