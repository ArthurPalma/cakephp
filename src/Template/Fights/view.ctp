<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight $fight
 */
?>
<style>
    .side-nav {
        height: 750px;
        /* Full-height: remove this if you want "auto" height */
        width: 300px;
        /* Set the width of the sidebar */
        position: fixed;
        /* Fixed Sidebar (stay in place on scroll) */
        left: 0;
        top: 0;
        background-color: #111;
        /* Black */
        overflow-x: hidden;
        /* Disable horizontal scroll */
        padding-top: 40px;
    }

    #nav ul {
        position: absolute;
        width: 15em;
        display: none;
        border: 1px solid #452915;
    }

    .tableau {
        width: 1100px;
        /* Set the width of the sidebar */
        right: 0;
        position: relative;
        top: 50px;
        left: 300px;
        font-style: monospace;
        font-weight: bold;
    }

    .titre {
        text-align: center;
        font-style: bold;
        font-size: 40px;
        background-color: white;
        /* Black */
    }

    .header {
        padding: 60px;
        text-align: center;
        background-color: #111;
        color: white;
        font-size: 30px;
        left: 300px;
        width: auto;
        top: 0;
        /* Stay at the top */
    }

    .pantheon {
        color: white;
        left: 300px;
    }

    @media screen and (max-width: 640px) {
        .side-nav {
            height: 750px;
            /* Full-height: remove this if you want "auto" height */
            width: 100px;
            /* Set the width of the sidebar */
            position: fixed;
            /* Fixed Sidebar (stay in place on scroll) */
            left: 0;
            top: 0;
            background-color: #111;
            /* Black */
            overflow-x: hidden;
            /* Disable horizontal scroll */
            padding-top: 20px;
        }

        #nav ul {
            position: absolute;
            width: 15em;
            display: none;
            border: 1px solid #452915;
        }

        .tableau {
            width: 150px;
            /* Set the width of the sidebar */
            right: 0;
            position: relative;
            top: 0px;
            left: 100px;
            font-style: monospace;
            font-weight: bold;
        }

        .titre {
            text-align: center;
            font-style: bold;
            font-size: 20px;
            background-color: white;
            /* Black */
        }

        .header {
            padding: 30px;
            text-align: center;
            background-color: #111;
            color: white;
            font-size: 15px;
            left: 100px;
            width: auto;
            top: 0;
            /* Stay at the top */
        }

        .pantheon {
            color: white;
            left: 100px;
        }
    }

    @media screen and (min-width: 200px) and (max-width: 640px) {

    }
</style>
<div class="header">
    <h1 class="pantheon">Pantheon des dresseurs</h1>
</div>
<nav>
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Fight'), ['action' => 'edit', $fight->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Fight'), ['action' => 'delete', $fight->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fight->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Fights'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fight'), ['action' => 'add']) ?> </li>
        <li><a href="http://localhost/cakephp/Dresseurs">Dresseurs</a></li>
        <li><a href="http://localhost/cakephp/DresseurPokemons">DresseurPokemons</a></li>
        <li><a href="http://localhost/cakephp/Pokes">Pokes</a></li>
        <li><a href="http://localhost/cakephp/Fights">Fights</a></li>
    </ul>
</nav>
<div class="tableau">
    <h3 class="titre"><?= h($fight->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($fight->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Dresseur Id') ?></th>
            <td><?= $this->Number->format($fight->first_dresseur_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Second Dresseur Id') ?></th>
            <td><?= $this->Number->format($fight->second_dresseur_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Winner Dresseur Id') ?></th>
            <td><?= $this->Number->format($fight->winner_dresseur_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($fight->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($fight->modified) ?></td>
        </tr>
    </table>
</div>