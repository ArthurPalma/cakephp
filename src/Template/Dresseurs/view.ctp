<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */
?>
<style>
    .side-nav {
        height: 750px;
        /* Full-height: remove this if you want "auto" height */
        width: 300px;
        /* Set the width of the sidebar */
        position: fixed;
        /* Fixed Sidebar (stay in place on scroll) */
        left: 0;
        top: 0;
        background-color: #111;
        /* Black */
        overflow-x: hidden;
        /* Disable horizontal scroll */
        padding-top: 40px;
    }

    #nav ul {
        position: absolute;
        width: 15em;
        display: none;
        border: 1px solid #452915;
    }

    .tableau {
        width: 1100px;
        /* Set the width of the sidebar */
        right: 0;
        position: relative;
        top: 50px;
        left: 300px;
        font-style: monospace;
        font-weight: bold;
    }

    .titre {
        text-align: center;
        font-style: bold;
        font-size: 40px;
        background-color: white;
        /* Black */
    }

    .header {
        padding: 60px;
        text-align: center;
        background-color: #111;
        color: white;
        font-size: 30px;
        left: 300px;
        width: auto;
        top: 0;
        /* Stay at the top */
    }

    .pantheon {
        color: white;
        left: 300px;
    }
</style>
<div class="header">
    <h1 class="pantheon">Pantheon des dresseurs</h1>
</div>
<nav>
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dresseur'), ['action' => 'edit', $dresseur->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dresseur'), ['action' => 'delete', $dresseur->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['controller' => 'DresseurPokemons', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['controller' => 'DresseurPokemons', 'action' => 'add']) ?> </li>
        <li><a href="http://localhost/cakephp/Dresseurs">Dresseurs</a></li>
        <li><a href="http://localhost/cakephp/DresseurPokemons">DresseurPokemons</a></li>
        <li><a href="http://localhost/cakephp/Pokes">Pokes</a></li>
        <li><a href="http://localhost/cakephp/Fights">Fights</a></li>
    </ul>
</nav>
<div class="tableau">
    <h3 class="titre"><?= h($dresseur->prenom) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nom') ?></th>
            <td><?= h($dresseur->nom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prenom') ?></th>
            <td><?= h($dresseur->prenom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseur->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseur->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseur->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Dresseur Pokemons') ?></h4>
        <?php if (!empty($dresseur->dresseur_pokemons)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Dresseur Id') ?></th>
                <th scope="col"><?= __('Poke Id') ?></th>
                <th scope="col"><?= __('Favoris') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dresseur->dresseur_pokemons as $dresseurPokemons): ?>
            <tr>
                <td><?= h($dresseurPokemons->id) ?></td>
                <td><?= h($dresseurPokemons->dresseur_id) ?></td>
                <td><?= h($dresseurPokemons->poke_id) ?></td>
                <td><?= h($dresseurPokemons->favoris) ?></td>
                <td><?= h($dresseurPokemons->created) ?></td>
                <td><?= h($dresseurPokemons->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DresseurPokemons', 'action' => 'view', $dresseurPokemons->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DresseurPokemons', 'action' => 'edit', $dresseurPokemons->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DresseurPokemons', 'action' => 'delete', $dresseurPokemons->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemons->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
