<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPokemon[]|\Cake\Collection\CollectionInterface $dresseurPokemons
 */
?>
<style>
    .side-nav {
        height: 750px;
        /* Full-height: remove this if you want "auto" height */
        width: 300px;
        /* Set the width of the sidebar */
        position: fixed;
        /* Fixed Sidebar (stay in place on scroll) */
        left: 0;
        top: 0;
        background-color: #111;
        /* Black */
        overflow-x: hidden;
        /* Disable horizontal scroll */
        padding-top: 40px;
    }

    #nav ul {
        position: absolute;
        width: 15em;
        display: none;
        border: 1px solid #452915;
    }

    .tableau {
        width: 1100px;
        /* Set the width of the sidebar */
        right: 0;
        position: relative;
        top: 50px;
        left: 300px;
        font-style: monospace;
        font-weight: bold;
    }

    .titre {
        text-align: center;
        font-style: bold;
        font-size: 40px;
        background-color: white;
        /* Black */
    }

    .header {
        padding: 60px;
        text-align: center;
        background-color: #111;
        color: white;
        font-size: 30px;
        left: 300px;
        width: auto;
        top: 0;
        /* Stay at the top */
    }

    .pantheon {
        color: white;
        left: 300px;
    }
</style>
<div class="header">
    <h1 class="pantheon">Pantheon des dresseurs</h1>

</div>
<nav>
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pokes'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Poke'), ['controller' => 'Pokes', 'action' => 'add']) ?></li>
        <li><a href="http://localhost/cakephp/Dresseurs">Dresseurs</a></li>
        <li><a href="http://localhost/cakephp/DresseurPokemons">DresseurPokemons</a></li>
        <li><a href="http://localhost/cakephp/Pokes">Pokes</a></li>
        <li><a href="http://localhost/cakephp/Fights">Fights</a></li>
    </ul>
</nav>
<div class="tableau">
    <table>
        <thead>
        <h3 class="titre"><?= __('Dresseur Pokemons') ?></h3>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dresseur_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('poke_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('favoris') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dresseurPokemons as $dresseurPokemon): ?>
            <tr>
                <td><?= $this->Number->format($dresseurPokemon->id) ?></td>
                <td><?= $dresseurPokemon->has('dresseur') ? $this->Html->link($dresseurPokemon->dresseur->id, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPokemon->dresseur->id]) : '' ?></td>
                <td><?= $dresseurPokemon->has('poke') ? $this->Html->link($dresseurPokemon->poke->id, ['controller' => 'Pokes', 'action' => 'view', $dresseurPokemon->poke->id]) : '' ?></td>
                <td><?= h($dresseurPokemon->favoris) ?></td>
                <td><?= h($dresseurPokemon->created) ?></td>
                <td><?= h($dresseurPokemon->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dresseurPokemon->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseurPokemon->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseurPokemon->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemon->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
