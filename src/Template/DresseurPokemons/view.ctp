<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPokemon $dresseurPokemon
 */
?>
<style>
    .side-nav {
        height: 750px;
        /* Full-height: remove this if you want "auto" height */
        width: 300px;
        /* Set the width of the sidebar */
        position: fixed;
        /* Fixed Sidebar (stay in place on scroll) */
        left: 0;
        top: 0;
        background-color: #111;
        /* Black */
        overflow-x: hidden;
        /* Disable horizontal scroll */
        padding-top: 40px;
    }

    #nav ul {
        position: absolute;
        width: 15em;
        display: none;
        border: 1px solid #452915;
    }

    .tableau {
        width: 1100px;
        /* Set the width of the sidebar */
        right: 0;
        position: relative;
        top: 50px;
        left: 300px;
        font-style: monospace;
        font-weight: bold;
    }

    .titre {
        text-align: center;
        font-style: bold;
        font-size: 40px;
        background-color: white;
        /* Black */
    }

    .header {
        padding: 60px;
        text-align: center;
        background-color: #111;
        color: white;
        font-size: 30px;
        left: 300px;
        width: auto;
        top: 0;
        /* Stay at the top */
    }

    .pantheon {
        color: white;
        left: 300px;
    }
</style>
<div class="header">
    <h1 class="pantheon">Pantheon des dresseurs</h1>
</div>
<nav>
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Dresseur Pokemon'), ['action' => 'edit', $dresseurPokemon->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dresseur Pokemon'), ['action' => 'delete', $dresseurPokemon->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemon->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['action' => 'add']) ?> </li>
        <li><a href="http://localhost/cakephp/Dresseurs">Dresseurs</a></li>
        <li><a href="http://localhost/cakephp/DresseurPokemons">DresseurPokemons</a></li>
        <li><a href="http://localhost/cakephp/Pokes">Pokes</a></li>
        <li><a href="http://localhost/cakephp/Fights">Fights</a></li>
    </ul>
</nav>
<div class="tableau">
    <h3 class="titre"><?= h($dresseurPokemon->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Dresseur') ?></th>
            <td><?= $dresseurPokemon->has('dresseur') ? $this->Html->link($dresseurPokemon->dresseur->id, ['controller' => 'Dresseurs', 'action' => 'view', $dresseurPokemon->dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Poke') ?></th>
            <td><?= $dresseurPokemon->has('poke') ? $this->Html->link($dresseurPokemon->poke->id, ['controller' => 'Pokes', 'action' => 'view', $dresseurPokemon->poke->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseurPokemon->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseurPokemon->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseurPokemon->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Favoris') ?></th>
            <td><?= $dresseurPokemon->favoris ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
