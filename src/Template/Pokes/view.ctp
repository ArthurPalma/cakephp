<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>
<style>
    .side-nav {
        height: 750px;
        /* Full-height: remove this if you want "auto" height */
        width: 300px;
        /* Set the width of the sidebar */
        position: fixed;
        /* Fixed Sidebar (stay in place on scroll) */
        left: 0;
        top: 0;
        background-color: #111;
        /* Black */
        overflow-x: hidden;
        /* Disable horizontal scroll */
        padding-top: 40px;
    }

    #nav ul {
        position: absolute;
        width: 15em;
        display: none;
        border: 1px solid #452915;
    }

    .tableau {
        width: 1100px;
        /* Set the width of the sidebar */
        right: 0;
        position: relative;
        top: 50px;
        left: 300px;
        font-style: monospace;
        font-weight: bold;
    }

    .titre {
        text-align: center;
        font-style: bold;
        font-size: 40px;
        background-color: white;
        /* Black */
    }

    .header {
        padding: 60px;
        text-align: center;
        background-color: #111;
        color: white;
        font-size: 30px;
        left: 300px;
        width: auto;
        top: 0;
        /* Stay at the top */
    }

    .pantheon {
        color: white;
        left: 300px;
    }
</style>
<div class="header">
    <h1 class="pantheon">Pantheon des dresseurs</h1>
</div>
<nav>
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Poke'), ['action' => 'edit', $poke->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Poke'), ['action' => 'delete', $poke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pokes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poke'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['controller' => 'DresseurPokemons', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['controller' => 'DresseurPokemons', 'action' => 'add']) ?> </li>
        <li><a href="http://localhost/cakephp/Dresseurs">Dresseurs</a></li>
        <li><a href="http://localhost/cakephp/DresseurPokemons">DresseurPokemons</a></li>
        <li><a href="http://localhost/cakephp/Pokes">Pokes</a></li>
        <li><a href="http://localhost/cakephp/Fights">Fights</a></li>
    </ul>
</nav>
<div class="tableau">
    <h3 class="titre"><?= h($poke->nom) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nom') ?></th>
            <td><?= h($poke->nom) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($poke->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pokedex Number') ?></th>
            <td><?= $this->Number->format($poke->pokedex_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Health') ?></th>
            <td><?= $this->Number->format($poke->health) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Attack') ?></th>
            <td><?= $this->Number->format($poke->attack) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Defense') ?></th>
            <td><?= $this->Number->format($poke->defense) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($poke->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($poke->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Dresseur Pokemons') ?></h4>
        <?php if (!empty($poke->dresseur_pokemons)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Dresseur Id') ?></th>
                <th scope="col"><?= __('Poke Id') ?></th>
                <th scope="col"><?= __('Favoris') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Is Fav') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($poke->dresseur_pokemons as $dresseurPokemons): ?>
            <tr>
                <td><?= h($dresseurPokemons->id) ?></td>
                <td><?= h($dresseurPokemons->dresseur_id) ?></td>
                <td><?= h($dresseurPokemons->poke_id) ?></td>
                <td><?= h($dresseurPokemons->favoris) ?></td>
                <td><?= h($dresseurPokemons->created) ?></td>
                <td><?= h($dresseurPokemons->modified) ?></td>
                <td><?= h($dresseurPokemons->is_fav) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DresseurPokemons', 'action' => 'view', $dresseurPokemons->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DresseurPokemons', 'action' => 'edit', $dresseurPokemons->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DresseurPokemons', 'action' => 'delete', $dresseurPokemons->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemons->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
