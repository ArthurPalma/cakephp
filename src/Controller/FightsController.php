<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $formData= $this->request->getData();
            $formData['winner_dresseur_id']= $this->_computerFight($formData);
        $fight = $this->Fights->patchEntity($fight,$formData);
            if ($this->Fights->save($fight)) {
                $this->Flash->success(__('The fight has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fight could not be saved. Please, try again.'));
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs'));
    }


    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    protected function _computerFight($data)
    {
            $pvpkm1=100;
            $pvpkm2=100;
            while ($pvpkm1 > 0 && $pvpkm2 > 0 )
            {
                $pvpkm1=$pvpkm1-rand(5,15);
                $pvpkm2=$pvpkm2-rand(5,15);
            }
            
            if($pvpkm1 > $pvpkm2){
                return $data['first_dresseur_id'];
            }
            if($pvpkm1 < $pvpkm2){
                return $data['second_dresseur_id'];
            }
            if($pvpkm1 == $pvpkm2){
                return $data['second_dresseur_id'];
            }

            /*$firstDresseur =$this->Fights->FirstDresseurs->get($data['first_dresseur_id'], [
                'contain'=> ['DresseurPokes, Pokes' => function ($query){
                return $query->where(['is_fav'=>true]);
                }]
            ]);
            $secDresseur =$this->Fights->FirstDresseurs->get($data['first_dresseur_id'], [
                'contain'=> ['DresseurPokes, Pokes' => function ($query){
                return $query->where(['is_fav'=>true]);
                }]
            ]);
            */
    }
}
