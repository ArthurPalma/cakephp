<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pokes Controller
 *
 * @property \App\Model\Table\PokesTable $Pokes
 *
 * @method \App\Model\Entity\Poke[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PokesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $pokes = $this->paginate($this->Pokes);

        $this->set(compact('pokes'));
    }

    /**
     * View method
     *
     * @param string|null $id Poke id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $poke = $this->Pokes->get($id, [
            'contain' => ['DresseurPokemons']
        ]);

        $this->set('poke', $poke);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $poke = $this->Pokes->newEntity();
        if ($this->request->is('post')) {
            $poke = $this->Pokes->patchEntity($poke, $this->request->getData());
            if ($this->Pokes->save($poke)) {
                $this->Flash->success(__('The poke has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The poke could not be saved. Please, try again.'));
        }
        $this->set(compact('poke'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Poke id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $poke = $this->Pokes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $poke = $this->Pokes->patchEntity($poke, $this->request->getData());
            if ($this->Pokes->save($poke)) {
                $this->Flash->success(__('The poke has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The poke could not be saved. Please, try again.'));
        }
        $this->set(compact('poke'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Poke id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $poke = $this->Pokes->get($id);
        if ($this->Pokes->delete($poke)) {
            $this->Flash->success(__('The poke has been deleted.'));
        } else {
            $this->Flash->error(__('The poke could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
